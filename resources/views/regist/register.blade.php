<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Matteo Bruni">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/sweetalert2/5.3.5/sweetalert2.min.css">
    <script src="sweetalert2.all.min.js"></script>
    <title>Register Template - tsParticles</title>

    <!-- Custom styles for this template -->
    <link href="css/particles.css" rel="stylesheet"/>
    <link href="css/auth.css" rel="stylesheet"/>
</head>
<body>
<div id="tsparticles"></div>
<main class="box">
    <h2>Register</h2>
    <form action="{{route('simpan_registrasi')}}" method="POST">
        {{csrf_field()}}
        <div class="inputBox">
            <label for="name">Name</label>
            <input type="text" name="name" id="name" placeholder="type your username" required/>
        </div>
        <div class="inputBox">
            <label for="email">Email</label>
            <input type="email" name="email" id="email" placeholder="example@email.com" required/>
        </div>
        <div class="inputBox">
            <label for="password">Password</label>
            <input type="password" name="password" id="password" placeholder="Password"
                   required/>
        </div>
        <button type="submit" name="" style="float: left;">Submit</button>
        <a class="button" href="login" style="float: left;">Login</a>
    </form>
</main>
<footer>
</footer>

<script src="https://cdn.jsdelivr.net/npm/tsparticles@1.34.1/tsparticles.min.js"
        integrity="sha256-D6LXCdCl4meErhc25yXnxIFUtwR96gPo+GtLYv89VZo=" crossorigin="anonymous"></script>
<script type="text/javascript" src="js/particles.js"></script>
<script src="https://cdn.jsdelivr.net/sweetalert2/5.3.5/sweetalert2.min.js"></script>
@include('sweetalert::alert')
</body>

</html>
