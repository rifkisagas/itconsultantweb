<!-- Core scripts -->
    <script src="{{asset('assets/js/pace.js')}}"></script>
    <script src="{{asset('assets/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('assets/libs/popper/popper.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.js')}}"></script>
    <script src="{{asset('assets/js/sidenav.js')}}"></script>
    <script src="{{asset('assets/js/layout-helpers.js')}}"></script>
    <script src="{{asset('assets/js/material-ripple.js')}}"></script>
    <script src="/js/app.js"></script>

    <!-- Libs -->
    <script src="{{asset('assets/libs/perfect-scrollbar/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('assets/libs/eve/eve.js')}}"></script>
    <script src="{{asset('assets/libs/flot/flot.js')}}"></script>
    <script src="{{asset('assets/libs/flot/curvedLines.js')}}"></script>
    <script src="{{asset('assets/libs/chart-am4/core.js')}}"></script>
    <script src="{{asset('assets/libs/chart-am4/charts.js')}}"></script>
    <script src="{{asset('assets/libs/chart-am4/animated.js')}}"></script>

    <!-- Demo-->
    <script src="{{asset('assets/js/demo.js')}}"></script> 
    <script src="assets/js/analytics.js"></script>
    <script src="{{asset('assets/js/pages/dashboards_index.js')}}"></script> 

    <!-- Sweet Alert-->
    <script src="https://cdn.jsdelivr.net/npm/tsparticles@1.34.1/tsparticles.min.js"
        integrity="sha256-D6LXCdCl4meErhc25yXnxIFUtwR96gPo+GtLYv89VZo=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/particles.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>