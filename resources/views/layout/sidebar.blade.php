            <div id="layout-sidenav" class="layout-sidenav sidenav sidenav-vertical bg-dark">
                <!-- Brand demo (see assets/css/demo/demo.css) -->
                <div class="app-brand demo">
                    <span class="app-brand-logo demo">
                        <img src="{{asset('assets/img/logo.png')}}" alt="Brand Logo" class="img-fluid">
                    </span>
                    <a href="index.html" class="app-brand-text demo sidenav-text font-weight-normal ml-2">IT Flow</a>
                    <a href="javascript:" class="layout-sidenav-toggle sidenav-link text-large ml-auto">
                        <i class="ion ion-md-menu align-middle"></i>
                    </a>
                </div>
                <div class="sidenav-divider mt-0"></div>

                <!-- Links -->
                <ul class="sidenav-inner py-1">

                    <!-- Dashboards -->
                    <li class="sidenav-item active">
                        <a href="/" class="sidenav-link">
                            <i class="sidenav-icon feather icon-home"></i>
                            <div>Dashboards</div>
                        </a>
                    </li>

                    <!-- Forms & Tables -->
                    <li class="sidenav-divider mb-1"></li>
                    <li class="sidenav-header small font-weight-semibold">Forms & Tables</li>
                    <li class="sidenav-item">
                        <a href="javascript:" class="sidenav-link sidenav-toggle">
                            <i class="sidenav-icon feather icon-clipboard"></i>
                            <div>Forms</div>
                        </a>
                        <ul class="sidenav-menu">
                        @if(auth()->user()->level=="admin")
                            <!-- ini buat admin -->
                            <li class="sidenav-item">
                                <a href="{{route('input_konsultan')}}" class="sidenav-link">
                                    <div>Tambah Konsultan</div>
                                </a>
                            </li>
                            <li class="sidenav-item">
                                <a href="{{route('getinput')}}" class="sidenav-link">
                                    <div>Tambah Artikel</div>
                                </a>
                            </li>
                            @endif
                     
                            <!-- ini buat user -->
                            <li class="sidenav-item">
                                <a href="{{route('input_konsultasi')}}" class="sidenav-link">
                                    <div>Form Daftar Konsultasi</div>
                                </a>
                            </li>
                        </ul>
                    </li>
                @if(auth()->user()->level=="admin")
                    <li class="sidenav-item">
                        <a href="{{route('data_konsultan')}}" class="sidenav-link">
                            <i class="sidenav-icon feather icon-grid"></i>
                            <div>Data Konsultan</div>
                        </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="{{route('data_artikel')}}" class="sidenav-link">
                            <i class="sidenav-icon feather icon-grid"></i>
                            <div>Data Artikel</div>
                        </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="{{route('data_konsultasi')}}" class="sidenav-link">
                            <i class="sidenav-icon feather icon-grid"></i>
                            <div>Data Konsultasi</div>
                        </a>
                    </li>
                @endif
                    <!-- Pages -->
                    <li class="sidenav-divider mb-1"></li>
                    <li class="sidenav-header small font-weight-semibold">Pages</li>
                    <li class="sidenav-item">
                        <a href="{{route('logout')}}" class="sidenav-link">
                            <i class="sidenav-icon feather icon-lock"></i>
                            <div>logout</div>
                        </a>
                    </li>
                    <!-- <li class="sidenav-item">
                        <a href="pages_authentication_register-v1.html" class="sidenav-link">
                            <i class="sidenav-icon feather icon-user"></i>
                            <div>Signup</div>
                        </a>
                    </li> -->
                </ul>
            </div>