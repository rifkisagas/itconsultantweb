@extends('layout.main')
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
@section('content') 
                <div class="container-fluid flex-grow-1 container-p-y">
                        <h4 class="font-weight-bold py-3 mb-0">Daftar Konsultan</h4>
                        <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i class="feather icon-home"></i></a></li>
                                <li class="breadcrumb-item" >Tables</li>
                                <li class="breadcrumb-item active" >Daftar Konsultan</li>
                            </ol>
                        </div>
                        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h1 class="modal-title fs-5" id="exampleModalLabel">Edit Data</h1>
                                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                {{-- ini bagian form modal, submit button udah work, tinggal api sama hubungin data --}}
                                <form action="{{url('api/input_konsultan')}}" method="POST">
                                    {{csrf_field()}}
                                    <div class="modal-body">
                                        <div class="card mb-4">
                                            <h6 class="card-header">Data Konsultan</h6>
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label class="form-label">Nama</label>
                                                    <input id="nama" name="nama" type="text" class="form-control" placeholder="...">
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label">Email</label>
                                                    <input id="email" name="email" type="email" class="form-control" placeholder="excample@gmail.com">
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label">Bidang</label>
                                                    <input id="bidang" name="bidang" type="text" class="form-control" placeholder="...">
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label">Alamat</label>
                                                    <input id="alamat" name="alamat" type="text" class="form-control" placeholder="...">
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label">Telpon</label>
                                                    <input id="telpon" name="telpon" type="text" class="form-control" placeholder="...">
                                                    <div class="clearfix"></div>
                                                </div>
                                            
                                                {{-- <button type="submit" name="" class="btn btn-primary">Submit</button> --}}
                                            </div>
                                      </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                        <button type="submit" name ="" class="btn btn-primary">Save changes</button>
                                    </div>
                                </form>
                              </div>
                            </div>
                        </div>
                 <hr class="border-light container-m--x my-4">
                        <h6 class="text-muted small font-weight-bold py-3 my-4">Table head options</h6>

                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Bidang</th>
                                    <th>Alamat</th>
                                    <th>Telepon</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($konsultan as $key => $data)
                                    <tr>
                                        <th scope="row">{{$key+1}}</th>
                                        <td>{{$data->nama}}</td>
                                        <td>{{$data->email}}</td>
                                        <td>{{$data->bidang}}</td>
                                        <td>{{$data->alamat}}</td>
                                        <td>{{$data->telpon}}</td>
                                        <td> 
                                            <form action="{{url('api/delete_konsultan', $data->id) }}" method="post" class="d-inline"> 
                                            @csrf <!-- berfungsi supaya saat pengiriman form tidak terjadi masalah -->
                                                 @method('delete') 
                                                <button class="btn  btn-sm show_confirm">
                                                    <i class="fas fa-trash-alt" darkgreen style="color: darkgreen ;"></i>
                                                </button>
                                             </form> 
                                              {{-- <!--{{ route('update', $data->id) }} --}}
                                            <a href="#">
                                                <button type="button" class="btn btn-sm show_update" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                                    <i class="fas fa-edit" style="color:darkgreen ;"></i>
                                                </button> 
                                            </a> 
                                      </td>
                                    </tr>
                                @endforeach
                                {{-- @foreach ($relasi as $us => $relasii) --}}
                                            {{-- // <option value="{{ $us->id }}">{{$us->nama}} -> {{$us->bidang}}</option> --}}
                                {{-- @endforeach --}}
                            </tbody>
                        </table>
                 </div>
                 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                 <script>
                    // Swal.fire('Any fool can use a computer')
                    const swalWithBootstrapButtons = Swal.mixin({
                        customClass: {
                            confirmButton: 'btn btn-success',
                            cancelButton: 'btn btn-danger'
                        },
                        buttonsStyling: false
                    })
                        $('.show_confirm').click(function(e) {
                            swalWithBootstrapButtons.fire({
                                title: 'Anda akan menghapus data konsultan!',
                                text: "Apakah anda yakin?",
                                icon: 'warning',
                                showCancelButton: true,
                                cancelButtonText: 'Tidak',
                                confirmButtonText: 'Ya, Hapus data',
                                reverseButtons: true
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    swalWithBootstrapButtons.fire(
                                        'Berhasil!',
                                        'Data Konsultan berhasil di hapus.',
                                        'success'
                                    )
                                    window.location.href = "{{url('api/delete_konsultan', $data->id)}}"
                                } 
                            })
                        });
                        // $('.show_update').click(function(e) {
                        // });
                </script>
                @include('sweetalert::alert')
@endsection