@extends('layout.main')
@section('content')
                    <div class="container-fluid flex-grow-1 container-p-y">
                        <h4 class="font-weight-bold py-3 mb-0">Tambah Data Konsultan</h4>
                        <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i class="feather icon-home"></i></a></li>
                                <li class="breadcrumb-item">Forms</li>
                                <li class="breadcrumb-item active">Input Konsultan</li>
                            </ol>
                        </div>
                        <div class="card mb-4">
                            <h6 class="card-header">Default</h6>
                            <div class="card-body">
                                <form action="{{url('api/input_konsultan')}}" method="POST">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label class="form-label">Nama</label>
                                        <input id="nama" name="nama" type="text" class="form-control" placeholder="...">
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Email</label>
                                        <input id="email" name="email" type="email" class="form-control" placeholder="excample@gmail.com">
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Bidang</label>
                                        <input id="bidang" name="bidang" type="text" class="form-control" placeholder="...">
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Alamat</label>
                                        <input id="alamat" name="alamat" type="text" class="form-control" placeholder="...">
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Telpon</label>
                                        <input id="telpon" name="telpon" type="text" class="form-control" placeholder="...">
                                        <div class="clearfix"></div>
                                    </div>
                                
                                    <button type="submit" name="" class="btn btn-primary">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- <script src="sweetalert2.all.min.js"></script> -->
                    @include('sweetalert::alert')
@endsection

