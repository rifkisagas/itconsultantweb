@extends('layout.main')

@section('content') 
                <div class="container-fluid flex-grow-1 container-p-y">
                        <h4 class="font-weight-bold py-3 mb-0">Data Konsultasi</h4>
                        <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i class="feather icon-home"></i></a></li>
                                <li class="breadcrumb-item" >Tables</li>
                                <li class="breadcrumb-item active" >Data Konsultasi</li>
                            </ol>
                        </div>
                 <hr class="border-light container-m--x my-4">
                        <h6 class="text-muted small font-weight-bold py-3 my-4">Table head options</h6>
                        <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Metode Konsultasi</th>
                                    <th>Tanggal Konsultasi</th>
                                    <th>Nama Konsultan</th>
                                    <th>Keterangan</th>
                                    <th>Aksi</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($posts as $key => $data)
                                    <tr>
                                        <th scope="row">{{$key+1}}</th>
                                        <td>{{$data->nama}}</td>
                                        <td>{{$data->email}}</td>
                                        <td>{{$data->metode_konsultasi}}</td>
                                        <td>{{$data->tanggal_konsultasi}}</td>
                                        <td>{{$data->konsultan->nama}}</td>
                                        <td> 
                                            @if($data->keterangan =='proses')
                                            <span class="badge rounded-pill bg-warning text-dark">
                                            @elseif($data->keterangan =='selesai')
                                            <span class="badge rounded-pill bg-success">
                                            @else
                                            <span>
                                            @endif
                                            {{$data->keterangan}}
                                            </span>
                                        </td>
                                        <td>
                                            <a href="{{ route('status_konsultasi',$data ->id) }}?status=selesai" class="btn btn-success btn-sm">
                                                <i class="fa fa-check"></i>
                                            </a>
                                            <a href="{{ route('status_konsultasi',$data->id) }}?status=proses" class="btn btn-warning btn-sm">
                                                    <i class="fa fa-times" aria-hidden="true"></i>
                                            </a> 
                                            
                                        </td>
                                        <td> 
                                           <form action="{{url('api/delete_konsultasi', $data->id) }}" method="post" class="d-inline"> 
                                             @csrf <!-- berfungsi supaya saat pengiriman form tidak terjadi masalah --> 
                                                @method('delete') 
                                                <button class="btn  btn-sm show_confirm">
                                                    <i class="fas fa-trash-alt" darkgreen style="color: darkgreen ;"></i>
                                                </button>
                                             </form>
                                              <!-- | 
                                            <a href="#"><button class="btn  btn-sm">
                                            <i class="fas fa-edit" style="color:darkgreen ;"></i>
                                            </button> </a>  -->
                                      </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                 </div>
                 </div>
                 @include('sweetalert::alert')

@endsection