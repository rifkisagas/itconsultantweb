@extends('layout.main')

@section('content') 
                    <div class="container-fluid flex-grow-1 container-p-y">
                        <h4 class="font-weight-bold py-3 mb-0">Tambah Data Artikel</h4>
                        <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i class="feather icon-home"></i></a></li>
                                <li class="breadcrumb-item">Forms</li>
                                <li class="breadcrumb-item active">Input Artikel</li>
                            </ol>
                        </div>
                        <div class="card mb-4">
                            <h6 class="card-header">Default</h6>
                            <div class="card-body">
                                <form action="{{url('api/input_artikel')}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="form-label">Nama Admin</label>
                                        <select name="nama_id" class="form-control">
                                        @foreach ($relasi as $us)
                                            <option value="{{ $us->id }}">{{$us->name}}</option>
                                        @endforeach
                                        </select>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Judul Artikel</label>
                                        <input id="judul_artikel" name="judul_artikel" type="text" class="form-control" placeholder="...">
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Isi Artikel</label>
                                        <input id="isi_artikel" name="isi_artikel" type="text" class="form-control" placeholder="...">
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label w-100">File input gambar</label>
                                        <input type="file"
                                            name="gambar"
                                            value="{{ old('gambar')}}"  
                                            accept="file/*"
                                            required
                                            class="">
                                        <small class="form-text text-muted">Example block-level help text here.</small>
                                    </div>
                                
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    @include('sweetalert::alert')

@endsection

