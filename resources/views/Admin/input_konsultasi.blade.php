@extends('layout.main')

@section('content') 
                    <div class="container-fluid flex-grow-1 container-p-y">
                        <h4 class="font-weight-bold py-3 mb-0">Daftar Konsultasi</h4>
                        <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i class="feather icon-home"></i></a></li>
                                <li class="breadcrumb-item">Forms</li>
                                <li class="breadcrumb-item active">Daftar Konsultasi</li>
                            </ol>
                        </div>
                        <div class="card mb-4">
                            <h6 class="card-header">Default</h6>
                            <div class="card-body">
                                <form action="{{ route('simpan_konsultasi')}}" method="POST">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label class="form-label">Nama</label>
                                        <input id="nama" name="nama" type="text" class="form-control" placeholder="...">
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Email</label>
                                        <input id="email" name="email" type="email" class="form-control" placeholder="excample@gmail.com">
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Metode Konsultasi</label>
                                         <select class="form-control" name="metode_konsultasi" required >
                                            <option value="Zoom">Zoom</option>
                                            <option value="Online">Online</option>
                                            <option value="Telpon">Telpon</option>
                                        </select>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Tanggal konsultasi</label>
                                        <input id="tanggal_konsultasi" name="tanggal_konsultasi" type="date" class="form-control">
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Nama Konsultan</label>
                                        <select name="konsultan_id" class="form-control">
                                        @foreach ($relasi as $us)
                                            <option value="{{ $us->id }}">{{$us->nama}} -> {{$us->bidang}}</option>
                                        @endforeach
                                        </select>
                                        <div class="clearfix"></div>
                                    </div>
                                
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- <script>
                        Swal.fire('Any fool can use a computer')
                    </script> -->
                    @include('sweetalert::alert')

@endsection

