@extends('layout.main')

@section('content') 
                    <div class="container-fluid flex-grow-1 container-p-y">
                        <h4 class="font-weight-bold py-3 mb-0">Tambah Data Konsultan</h4>
                        <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i class="feather icon-home"></i></a></li>
                                <li class="breadcrumb-item">Forms</li>
                                <li class="breadcrumb-item active">Input Konsultan</li>
                            </ol>
                        </div>
                        <div class="card mb-4">
                            <h6 class="card-header">Default</h6>
                            <div class="card-body">
                            
                                <form action="{{ route('update_konsultan',['id' =>$data->id])}}" method="POST">
                                @method('PUT')

                                {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="form-label">Nama</label>
                                        <label for="nama_subdomain" class="form-control-label">Nama Sub Domain</label>
                                        <input type="text"
                                            name="nama"
                                            value="{{ old('nama') ? old('nama') : $data->nama}}"  
                                            class="form-control @error('nama') is-invalid @enderror"/>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Email</label>
                                        <!-- <input id="email" name="email" type="email" class="form-control" placeholder="excample@gmail.com"> -->
                                        <input  type="email"
                                                name="email"
                                                value="{{ old('email') ? old('email') : $data->email }}"  
                                                class="form-control @error('email') is-invalid @enderror"/>
                                        <div class="clearfix"></div>  
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Bidang</label>
                                        <!-- <input id="bidang" name="bidang" type="text" class="form-control" placeholder="..."> -->
                                        <input  type="text"
                                                name="bidang"
                                                value="{{ old('bidang') ? old('bidang') : $data->bidang }}"  
                                                class="form-control @error('bidang') is-invalid @enderror"/>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Alamat</label>
                                        <!-- <input id="alamat" name="alamat" type="text" class="form-control" placeholder="..."> -->
                                        <input  type="text"
                                                name="alamat"
                                                value="{{ old('alamat') ? old('alamat') : $data->alamat }}"  
                                                class="form-control @error('alamat') is-invalid @enderror"/>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Telpon</label>
                                        <!-- <input id="telpon" name="telpon" type="text" class="form-control" placeholder="..."> -->
                                        <input  type="text"
                                                name="telpon"
                                                value="{{ old('telpon') ? old('telpon') : $data->telpon }}"  
                                                class="form-control @error('telpon') is-invalid @enderror"/>
                                        <div class="clearfix"></div>
                                    </div>
                                
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    @include('sweetalert::alert')

@endsection

