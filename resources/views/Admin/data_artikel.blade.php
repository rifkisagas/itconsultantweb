@extends('layout.main')

@section('content') 
                <div class="container-fluid flex-grow-1 container-p-y">
                        <h4 class="font-weight-bold py-3 mb-0">Data Artikel</h4>
                        <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i class="feather icon-home"></i></a></li>
                                <li class="breadcrumb-item" >Tables</li>
                                <li class="breadcrumb-item active" >Data Artikel</li>
                            </ol>
                        </div>
                 <hr class="border-light container-m--x my-4">
                        <h6 class="text-muted small font-weight-bold py-3 my-4">Table head options</h6>
                        <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                    <th>#</th>
                                    <th>Nama Admin</th>
                                    <th>Judul Artikel</th>
                                    <th>Isi Artikel</th>
                                    <th>Gambar</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($posts as $key => $data)
                                    <tr>
                                        <th scope="row">{{$key+1}}</th>
                                        <td>{{$data->user->name}}</td>
                                        <td>{{$data->judul_artikel}}</td>
                                        <td>{{$data->isi_artikel}}</td>
                                        <td>
                                        <!-- {{$data->gambar}} -->
                                        <img src="{{ asset('assets' . '/' . $data->gambar) }}" class="rounded" style="width: 50px">
                                       
                                        </td>
                                        <td> 
                                           <form action="{{url('api/delete_artikel', $data->id) }}" method="post" class="d-inline"> 
                                             @csrf <!-- berfungsi supaya saat pengiriman form tidak terjadi masalah --> 
                                                @method('delete') 
                                                <button class="btn  btn-sm show_confirm">
                                                    <i class="fas fa-trash-alt" darkgreen style="color: darkgreen ;"></i>
                                                </button>
                                             </form> | 
                                            <a href="#"><button class="btn  btn-sm">
                                            <i class="fas fa-edit" style="color:darkgreen ;"></i>
                                            </button> </a> 
                                      </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                 </div>
                 </div>
                 @include('sweetalert::alert')

@endsection