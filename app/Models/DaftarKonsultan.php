<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class DaftarKonsultan extends Model
{
    use HasFactory;
    protected $fillable = [
        'id','nama','email','bidang','alamat','telpon'

     ];
 
     protected $hidden = [
        
     ];

    
}
