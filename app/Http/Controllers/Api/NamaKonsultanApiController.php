<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Api\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\DaftarKonsultan;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Test\Constraint\ResponseFormatSame;
use RealRashid\SweetAlert\Facades\Alert;

class NamaKonsultanApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    
        $posts = DaftarKonsultan::latest()->get();
        return response([
            'success' => true,
            'message' => 'List Semua Posts',
            'data' => $posts
        ], 200);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data= new DaftarKonsultan();
        $data->nama = $request->nama;
        $data->email = $request->email;
        $data->bidang = $request->bidang;
        $data->alamat = $request->alamat;
        $data->telpon = $request->telpon;
        $data->save();

        // return response([
        //     'success' => true,
        //     'message' => 'List Semua Posts',
        //     'data' => $data
        // ], 200);
        return redirect('data_konsultan');
        // return redirect('data_konsultan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DaftarKonsultan::all();
        return view('Admin.update_konsultan')->with([
            'data'=>$data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = DaftarKonsultan::findOrFail($id);
        $data->update($request->all());
        return response([
            'success' => true,
            'message' => 'Data Berhasil di Update',
            'data' => $data
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            // $this->buildXMLHeader();
            $data = DaftarKonsultan::findOrFail($id);
            $data->delete();
            // return response([
            //     'success' => true,
            //     'message' => 'Data Berhasil Dihapus'
            // ], 200);
            return redirect()->route('data_konsultan')->with('toast_success', 'Data Berhasil Dihapus!');
        } catch  (\Exception $e)  {
            report($e);
            // echo "<script>setTimeout(function(){ }, 3000);</script>";
            return back()->with('error', 'Error menghapus data!');
        }
    }
}
