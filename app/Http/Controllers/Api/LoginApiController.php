<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use RealRashid\SweetAlert\Facades\Alert;

class LoginApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = User::all();
        return response([
            'success' => true,
            'message' => 'List Semua Posts',
            'data' => $posts
        ], 200);
    }

    public function Login(Request $request)
    {
        if(Auth::attempt($request->only('email','password'))){
            // return response([
            //     'success' => true,
            //     'message' => 'Anda Berhasil Login',
            //     'data' => $request
            // ], 200);
            Alert::success('Congrats', 'You\'ve Successfully Registered');
            return redirect()->back()->with(Alert::success('success', 'Created successfully!'));
        }else{
            return redirect()->back()->with('error', 'Login Failed!');
        }
        
       
     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data= new User();
        $data->name = $request->name;
        $data->level = 'user';
        $data->email = $request->email;
        $data->password = bcrypt($request->password);
        $data->remember_token = Str::random(60);
       
        $data->save();

        // $notification = array(
        //     'message' => 'Berhasil',
        //     'alert-type' => 'success',
        // );
        alert()->success('It worked!', 'The form was submitted');
        return back();
        // return redirect()->back()->with('success', 'Created successfully!');
        // return redirect()->route('regist.login')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
