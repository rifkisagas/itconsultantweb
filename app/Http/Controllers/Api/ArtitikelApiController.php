<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Artikel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;


class ArtitikelApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Artikel::with('user')->latest()->get();
        return view('Admin.data_artikel')->with([
            'posts' => $posts 
        ]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $relasi = User::all();
        $item = Artikel::with('user')->get();

        return view('Admin.input_artikel',compact('item','relasi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data= new Artikel();
        $data->nama_id = $request->nama_id;
        $data->judul_artikel = $request->judul_artikel;
        $data->isi_artikel = $request->isi_artikel;
        // $data->gambar = $request->gambar;

        // $data['gambar'] = $request->file('gambar')->store(
        //     'assets','public'
        // );

        $file = $request->gambar;
        $filename=date("Y-m-d",time()).'.'.$file->getClientOriginalName();
        $request->gambar->move('assets',$filename);
        $data->gambar=$filename;

        $data->save();

        return redirect('data_artikel');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Artikel::findOrFail($id);
        // $file =  $request['gambar'];
        // $filename=date("Y-m-d",time()).'.'.$file->getClientOriginalName();
        // $request->gambar->move('assets',$filename);
        // $dt = [
        //     'nama_id' => $request['nama_id'],
        //     'judul_artikel' => $request['judul_artikel'],
        //     'isi_artikel' => $request['isi_artikel'],
        // ];  

       
        // $data->update($dt);

        if ($request->hasFile('gambar')) {

            //upload image
            $image = $request->file('gambar');
            $image->storeAs('public/posts', $image->hashName());

            //delete old image
            Storage::delete('public/assets/'.$data->gambar);

            //update post with new image
            $data->update([
                'nama_id' => $request->nama_id,
                'judul_artikel' => $request->judul_artikel,
                'isi_artikel' => $request->isi_artikel,
                'gambar'     => $image->hashName(),
               
            ]);

        } else {

            //update post without image
            $data->update([
                'nama_id' => $request->nama_id,
                'judul_artikel' => $request->judul_artikel,
                'isi_artikel' => $request->isi_artikel,
            ]);
        }
        return response([
            'success' => true,
            'message' => 'Data Berhasil di Update',
            'data' => $data
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Artikel::findOrFail($id);
        $item->delete();
        // return response([
        //     'success' => true,
        //     'message' => 'Data Berhasil Dihapus'
        // ], 200);

        return redirect('data_artikel');
    }
}
