<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DaftarKonsultan;

class KelolaKonsultan extends Controller
{
     public function index()
    {
        $konsultan = DaftarKonsultan::all();

        return view('Admin.daftar_konsultan', compact('konsultan'));
    }
}
