<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Request;

class loginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('regist.login');
    }

    public function postlogin(Request $request){
         
        if(Auth::attempt($request->only('email','password'))){
            Alert::success('Anda Berhasil Login');
            return redirect('dashboard');
        }
        else{
        //    Alert::error('flash_message_eror','Email atau Password anda salah!!');
            Alert::error('Login gagal!', 'silahkan registrasi terlebih dahulu!');
            return redirect('registrasi');
        }
        
    //    dd($request->all());
     }

    public function logout (Request $request){
        Auth::logout();
        return redirect('/');
    }

    public function Registrasi(){
        return view('regist.register');

    }

    public function simpanregistrasi(Request $request){
        User::create([
            'name' => $request->name,
            'level' => 'user',
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'remember_token' => Str::random(60),
            
        ]);
        Alert::success('Anda Berhasil Registrasi', 'Silahkan login!');
        return redirect('login');
    }
}
