<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class AkunSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Muhamad Rifki Arisagas',
            'level' => 0,
            'email' => 'arisagasr@gmail.com',
            'email_verified_at' => \Carbon\Carbon::now(),
            'password' => '12345678',
            'remember_token' => 1,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
    }
}
