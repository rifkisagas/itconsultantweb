<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ArtitikelApiController;
use App\Http\Controllers\Api\NamaKonsultanApiController;
use App\Http\Controllers\Api\KonsultasiApiController;
use App\Http\Controllers\Api\LoginApiController;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/artikel', [ArtitikelApiController::class, 'index'])->name('artikel');
Route::get('/getinput', [ArtitikelApiController::class, 'create'])->name('getinput');
Route::post('/input_artikel', [ArtitikelApiController::class, 'store'])->name('input_artikel');
Route::put('/update_artikel/{id}', [ArtitikelApiController::class, 'update'])->name('update_artikel');
Route::delete('/delete_artikel/{id}', [ArtitikelApiController::class, 'destroy'])->name('delete_artikel');

Route::get('/konsultan', [NamaKonsultanApiController::class, 'index'])->name('konsultan');
Route::post('/input_konsultan', [NamaKonsultanApiController::class, 'store'])->name('input_konsultan');
Route::get('/update/{id}/edit', [NamaKonsultanApiController::class, 'edit'])->name('update');
Route::put('/update_konsultan/{id}', [NamaKonsultanApiController::class, 'update'])->name('update_konsultan');
Route::get('/delete_konsultan/{id}', [NamaKonsultanApiController::class, 'destroy'])->name('delete_konsultan');


Route::get('/konsultasi', [KonsultasiApiController::class, 'index'])->name('konsultasi');
Route::post('/input_konsultasi', [KonsultasiApiController::class, 'store'])->name('input_konsultasi');
Route::delete('/delete_konsultasi/{id}', [KonsultasiApiController::class, 'destroy'])->name('delete_konsultasi');

Route::get('/DataUser', [LoginApiController::class, 'index'])->name('DataUser');
Route::post('/Register', [LoginApiController::class, 'store'])->name('Register');
Route::post('/Login', [LoginApiController::class, 'Login'])->name('Login'); 