<?php

use App\Http\Controllers\ArtikelController;
use App\Http\Controllers\DashbordController;
use App\Http\Controllers\KonsultasiController;
use App\Http\Controllers\loginController;
use App\Http\Controllers\NamaKonsultanController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Front-end routes
// Route::get('/', function () {
//     return view('index');
// });

Route::get('/', [DashbordController::class, 'index']);

Route::get('/login', function () {
    return view('regist.login');
})->name('regist.login');

Route::get('/register', function () {
    return view('regist.register');
});

// Admin routes
Route::get('/admin', function () {
    return view('Admin.dashboard');
});

Route::get('/data_konsultan', [App\Http\Controllers\KelolaKonsultan::class, 'index'])->name('data_konsultan');

// Route::get('/data_artikel', function () {
//     return view('Admin.data_artikel');
// })->name('data_artikel');

Route::get('/input_artikel', function () {
    return view('Admin.input_artikel');
})->name('input_artikel');

// Route::get('/input_konsultan', function () {
//     return view('Admin.input_konsultan');
// })->name('input_konsultan');

Route::get('/input_konsultasi', function () {
    return view('Admin.input_konsultasi');
})->name('input_konsultasi');

// Route::get('/justapage', function () {
//     alert()->success('It worked!', 'The form was submitted');
//     return back();
// });
Route::get('/login', [loginController::class, 'index'])->name('login');
Route::post('/postlogin', [loginController::class, 'postlogin'])->name('postlogin');
Route::get('/registrasi', [loginController::class, 'Registrasi'])->name('registrasi');
Route::post('/simpan_registrasi', [loginController::class, 'simpanregistrasi'])->name('simpan_registrasi');
Route::get('/logout', [loginController::class, 'logout'])->name('logout');


Route::group(['middleware'=>['auth','CekLevel:admin']], function() {    
    Route::get('/artikel', [ArtikelController::class, 'index'])->name('artikel');
    Route::get('/getinput', [ArtikelController::class, 'create'])->name('getinput');

    Route::get('/input_konsultan', function () {
        return view('Admin.input_konsultan');
    })->name('input_konsultan');
    // Route::post('/input_konsultant', [NamaKonsultanApiController::class, 'store'])->name('input_konsultan');
    Route::get('/update/{id}/edit', [NamaKonsultanController::class, 'edit'])->name('update');
    Route::put('/update_konsultan/{id}', [NamaKonsultanController::class, 'update'])->name('update_konsultan');

});

Route::group(['middleware'=>['auth','CekLevel:admin,user']], function() {
    Route::get('/dashboard', function () {
        return view('Admin.dashboard');
    })->name('dashboard');
    
    Route::get('/data_konsultan', [App\Http\Controllers\KelolaKonsultan::class, 'index'])->name('data_konsultan');
    Route::get('/data_artikel', [ArtikelController::class, 'index'])->name('data_artikel');
    
    Route::get('/input_konsultasi', [KonsultasiController::class, 'create'])->name('input_konsultasi');
    Route::post('/simpan_konsultasi', [KonsultasiController::class, 'store'])->name('simpan_konsultasi');
    Route::get('/data_konsultasi', [KonsultasiController::class, 'index'])->name('data_konsultasi');
    Route::get('/status_konsultasi/{id}/set-status', [KonsultasiController::class, 'setStatus'])->name('status_konsultasi');
    

});

Route::get('/old', function () {
    return view('index-old');
});